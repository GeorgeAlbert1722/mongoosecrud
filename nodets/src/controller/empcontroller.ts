import { Request, Response } from 'express';
import { EmpService } from '../service/empservice';
import { EmployeeModel } from '../model/empmodel';

let empservice: EmpService;

export class empController {
    constructor() {
        empservice = new EmpService();
    }
    async empget(req: Request, res: Response) {
    
        try {
            const employee: EmployeeModel = await empservice.empget(req.body);
            res.status(200).send(employee);
        } catch (err) {
    
            if (err && err._message && err._message === 'validation failed') {
                res.status(400).send(err);
            } else {
                res.status(500).send(err);
            }
        }
    }
    async empcreate(req: Request, res: Response) {
        try {
            const employee: EmployeeModel = await empservice.empcreate(req.body);
            res.status(200).send(employee);
        } catch (err) {
            if (err && err._message && err._message === 'validation failed') {
                res.status(400).send(err);
            } else {
                res.status(500).send(err);
            }
        }
    }
    async empread(req: Request, res: Response) {
       
    try {
        const employee: EmployeeModel = await empservice.empread(req.body);
         res.status(200).send(employee);
    } catch (err) {
        if (err && err._message && err._message === 'validation failed') {
            res.status(400).send(err);
        } else {
            res.status(500).send(err);
        }
    }
}


async empupdate(req: Request, res: Response) {
    
    try {
        const employee: EmployeeModel = await empservice.empupdate(req.body);
        res.status(200).send(employee);
    } catch (err) {

        if (err && err._message && err._message === 'validation failed') {
            res.status(400).send(err);
        } else {
            res.status(500).send(err);
        }
    }
}
async empdelete(req: Request, res: Response) {
       
    try {
        await empservice.empdelete(req.body);
         res.status(204).send({data: "Deleted"});
    } catch (err) {
        
        if (err && err._message && err._message === 'validation failed') {
            res.status(400).send(err);
        } else {
            res.status(500).send(err);
        }
    }
}


}
