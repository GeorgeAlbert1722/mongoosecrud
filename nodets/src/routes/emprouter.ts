import express from 'express';
import { empController } from '../controller/empcontroller';

let empcontroller: empController;

export class empRoute {
    constructor() {
        empcontroller = new empController();
    }
    empRouter(app: express.Application) {
        app.get('/emp/get',empcontroller.empget);
        app.post('/emp', empcontroller.empcreate);
        app.get('/emp/:id',empcontroller.empread);
        app.put('/ud',empcontroller.empupdate);
        app.put('/emp/:id',empcontroller.empdelete);
    }
}