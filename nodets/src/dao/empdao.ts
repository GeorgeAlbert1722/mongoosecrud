import mongoose from 'mongoose';
import { empSchema, EmployeeModel } from '../model/empmodel';

const EmployeeModel = mongoose.model('Student', empSchema);
export class empdao {
    async getall(body: EmployeeModel){
        const employee = new EmployeeModel(body);
        return await employee;
    }
    async save(body: EmployeeModel) {
        const employee = new EmployeeModel(body);
         return await employee.save();
    }
    async getById(id:String){
        const employee = new EmployeeModel(id);
        return await employee;
    }
    async put(body : EmployeeModel){
        const employee = new EmployeeModel(body);
        return await employee;
    }
    async findByIdAndDelete(id:String){
        const employee = new EmployeeModel(id);
        //return await employee;
    }
}