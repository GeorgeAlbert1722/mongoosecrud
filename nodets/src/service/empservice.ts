import { empdao } from "../dao/empdao";
import { EmployeeModel } from "../model/empmodel";

let emp: empdao;
export class EmpService {
    constructor() {
        emp = new empdao();
    }
    async empget(body: EmployeeModel) {
        return await emp.getall(body);
    }
    
    async empcreate(body: EmployeeModel) {
        return await emp.save(body);
    }
    async empread(id:String) {
        return await emp.getById(id);
}
async empupdate(body: EmployeeModel) {
    return await emp.put(body);}
    
async empdelete(id:String) {
        return await emp.findByIdAndDelete(id);
}
}