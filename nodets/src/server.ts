import cors from 'cors';
import express from 'express';
import { MongoConfig } from './config/MongoConfig'
import { empRoute } from './routes/emprouter';
import bodyParser from 'body-parser';

const app: express.Application = express();
const employee = new empRoute();
app.use(cors());
app.use(bodyParser.json());
new MongoConfig();
app.get('/', (req, res) => {
    res.send({status: "Success"});
});
employee.empRouter(app);
app.listen(4000, () => {
    console.log('App is listening on port 4000');
});